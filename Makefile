all: pull package upload

pull:
	cd eoq2/ && git pull origin master

package:
	python3 setup.py sdist bdist_wheel

upload:
	twine upload -r ils dist/*
